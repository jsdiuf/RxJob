## 平台简介

> 我不是创造者，只是代码搬运工。

Rxjob 是基于xxl-job和ruoyi两个框架重构出来符合企业级项目管理任务调度平台。自从上线该项目两年以来，没有重大事故，也从未重启过，接入执行器50多个，运转任务300多个。足以说明这两个主流开源项目稳定性了。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0304/105409_53dfa12a_346157.png "屏幕截图.png")

## ruoyi 功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 任务调度：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql)支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

> 若依项目文档传送门： https://doc.ruoyi.vip/

## 任务调度平台说明

> `重要说明`: 
（1）建立的执行器必须与线上部署的项目一直才行，不容许乱起名字
（2）建立任务的名称，必须指明执行时间，方便了解执行情况
（3）对运行时间较长的任务，能异步就异步，不要使用同步操作
（4）项目日志不要过分依赖调度平台日志，调度器只打印必要的日志

## 介绍
### 1.1 概述
任务调度平台是基于开源项目**xxl-job**进行重构的项目

- 四步快速接入，代码与业务解耦
- 多点部署，高可用
- 支持对用户、部门、岗位等权限控制
- 支持实时任务监控
- 支持多重策略任务调度
- 支持实时在线日志追踪
- 支持预警提醒


### 1.2 特性

	1、简单：支持通过Web页面对任务进行CRUD操作，操作简单，一分钟上手；
	2、动态：支持动态修改任务状态、启动/停止任务，以及终止运行中任务，即时生效；
	3、调度中心HA（中心式）：调度采用中心式设计，“调度中心”基于集群Quartz实现并支持集群部署，可保证调度中心HA；
	4、执行器HA（分布式）：任务分布式执行，任务"执行器"支持集群部署，可保证任务执行HA；
	5、注册中心: 执行器会周期性自动注册任务, 调度中心将会自动发现注册的任务并触发执行。同时，也支持手动录入执行器地址；
	6、弹性扩容缩容：一旦有新执行器机器上线或者下线，下次调度时将会重新分配任务；
	7、路由策略：执行器集群部署时提供丰富的路由策略，包括：第一个、最后一个、轮询、随机、一致性HASH、最不经常使用、最近最久未使用、故障转移、忙碌转移等；
	8、故障转移：任务路由策略选择"故障转移"情况下，如果执行器集群中某一台机器故障，将会自动Failover切换到一台正常的执行器发送调度请求。
	9、阻塞处理策略：调度过于密集执行器来不及处理时的处理策略，策略包括：单机串行（默认）、丢弃后续调度、覆盖之前调度；
	10、任务超时控制：支持自定义任务超时时间，任务运行超时将会主动中断任务；
	11、任务失败重试：支持自定义任务失败重试次数，当任务失败时将会按照预设的失败重试次数主动进行重试；其中分片任务支持分片粒度的失败重试；
	12、任务失败告警；默认提供邮件方式失败告警，同时预留扩展接口，可方面的扩展短信、钉钉等告警方式；
	13、分片广播任务：执行器集群部署时，任务路由策略选择"分片广播"情况下，一次任务调度将会广播触发集群中所有执行器执行一次任务，可根据分片参数开发分片任务；
	14、动态分片：分片广播任务以执行器为维度进行分片，支持动态扩容执行器集群从而动态增加分片数量，协同进行业务处理；在进行大数据量业务操作时可显著提升任务处理能力和速度。
	15、事件触发：除了"Cron方式"和"任务依赖方式"触发任务执行之外，支持基于事件的触发任务方式。调度中心提供触发任务单次执行的API服务，可根据业务事件灵活触发。
	16、任务进度监控：支持实时监控任务进度；
	17、Rolling实时日志：支持在线查看调度结果，并且支持以Rolling方式实时查看执行器输出的完整的执行日志；
	18、GLUE：提供Web IDE，支持在线开发任务逻辑代码，动态发布，实时编译生效，省略部署上线的过程。支持30个版本的历史版本回溯。
	19、脚本任务：支持以GLUE模式开发和运行脚本任务，包括Shell、Python、NodeJS、PHP、PowerShell等类型脚本;
	20、命令行任务：原生提供通用命令行任务Handler（Bean任务，"CommandJobHandler"）；业务方只需要提供命令行即可；
	21、任务依赖：支持配置子任务依赖，当父任务执行结束且执行成功后将会主动触发一次子任务的执行, 多个子任务用逗号分隔；
	22、一致性：“调度中心”通过DB锁保证集群分布式调度的一致性, 一次任务调度只会触发一次执行；
	23、自定义任务参数：支持在线配置调度任务入参，即时生效；
	24、调度线程池：调度系统多线程触发调度运行，确保调度精确执行，不被堵塞；
	25、数据加密：调度中心和执行器之间的通讯进行数据加密，提升调度信息安全性；
	26、邮件报警：任务失败时支持邮件报警，支持配置多邮件地址群发报警邮件；
	27、推送maven中央仓库: 将会把最新稳定版推送到maven中央仓库, 方便用户接入和使用;
	28、运行报表：支持实时查看运行数据，如任务数量、调度次数、执行器数量等；以及调度报表，如调度日期分布图，调度成功分布图等；
	29、全异步：任务调度流程全异步化设计实现，如异步调度、异步运行、异步回调等，有效对密集调度进行流量削峰，理论上支持任意时长任务的运行；
	30、跨平台：原生提供通用HTTP任务Handler（Bean任务，"HttpJobHandler"）；业务方只需要提供HTTP链接即可，不限制语言、平台；
	31、国际化：调度中心支持国际化设置，提供中文、英文两种可选语言，默认为中文；
	32、容器化：提供官方docker镜像，并实时更新推送dockerhub，进一步实现产品开箱即用；

> xxl-job 项目传送门： https://www.xuxueli.com/xxl-job/
	

## 2 执行器开发

> 如果不建执行器可以使用 /doc/dlbe-job-excutor.rar 解压使用即可。自建执行器可以按照下面步骤进行创建即可。


### 步骤一：maven依赖
确认pom文件中引入了 "xxl-job-core" 的maven依赖；

```
<!-- xxl-job-core -->
<dependency>
	<groupId>com.xuxueli</groupId>
	<artifactId>xxl-job-core</artifactId>
	<version>2.0.1</version>
</dependency>
```
>注意：现在`xxl-job-core`低于2.0.1版本以下的有一个隐形bug，因为默认连接使用jetty连接，导致jetty重启断开连接，导致任务执行失败，修复bug,需`xxl-job-core` 2.0.2默使用`netty`连接,执行需添加一下配置

```
//父级pom.xml
<properties>
<jetty-server.version>9.2.26.v20180806</jetty-server.version>
</properties>

<dependencyManagement>
        <dependencies>
           ....

            <!-- jetty -->
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-server</artifactId>
                <version>${jetty-server.version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-util</artifactId>
                <version>${jetty-server.version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-http</artifactId>
                <version>${jetty-server.version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-io</artifactId>
                <version>${jetty-server.version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-client</artifactId>
                <version>${jetty-server.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>
```

 

### 步骤二：修改执行器地址

```
### xxl-job admin address list：调度中心部署跟地址：如调度中心集群部署存在多个地址则用逗号分隔。执行器将会使用该地址进行"执行器心跳注册"和"任务结果回调"。
xxl.job.admin.addresses=http://task.cdeledu.com

### xxl-job executor address：执行器"AppName"和地址信息配置：AppName执行器心跳注册分组依据；地址信息用于"调度中心请求并触发任务"和"执行器注册"。执行器默认端口为9999，执行器IP默认为空表示自动获取IP，多网卡时可手动设置指定IP，该IP不会绑定Host仅作为通讯实用。单机部署多个执行器时，注意要配置不同执行器端口；
xxl.job.executor.appname=xxl-job-executor-sample
xxl.job.executor.ip=
xxl.job.executor.port=9999

### xxl-job, access token：执行器通讯TOKEN，非空时启用
xxl.job.accessToken=

### xxl-job log path：执行器运行日志文件存储的磁盘位置，需要对该路径拥有读写权限
xxl.job.executor.logpath=/data/applogs/xxl-job/jobhandler/

### xxl-job log retention days：执行器Log文件定期清理功能，指定日志保存天数，日志文件过期自动删除。限制至少保持3天，否则功能不生效；
xxl.job.executor.logretentiondays=-1
```

**springboot-  yml版**
```
# 执行器配置
xxl:
  job:
    enabled: false  #控制是否开启任务
    executor:
      logpath: /home/applogs/dlbe-job-executor/jobhandler #项目本地执行地址
      appname: cdeledu-live-manage-excuor # 执行器名字
      port: 9977
      logretentiondays: -1
      ip: 
    admin:
      addresses: http://task.xxx.com #任务调度平台
    accessToken: jobtest$123 # 任务调度平台唯一标识

```
>**accessToken：需要配置对应的accessToken才行（线上：`jobprod$123`，本地和测试：`jobtest$123`）**
大家的port端口号都统一暴露：9977，方便运维那边统计处理

### 步骤三： 初始化基础配置

```
package com.cdeledu.job.excutor.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;

/**
 * xxl-job config
 *
 * @author wuchanghao 2019-03-27
 */
@Configuration
@ComponentScan(basePackages = "com.cdeledu.job.excutor.service.jobhandler")
public class XxlJobConfig {
	private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

	// 执行器开关
	@Value("${xxl.job.enabled}")
	private boolean enabled;

	@Value("${xxl.job.admin.addresses}")
	private String adminAddresses;

	@Value("${xxl.job.executor.appname}")
	private String appName;

	@Value("${xxl.job.executor.ip}")
	private String ip;

	@Value("${xxl.job.executor.port}")
	private int port;

	@Value("${xxl.job.accessToken}")
	private String accessToken;

	@Value("${xxl.job.executor.logpath}")
	private String logPath;

	@Value("${xxl.job.executor.logretentiondays}")
	private int logRetentionDays;

	@Bean(initMethod = "start", destroyMethod = "destroy")
	public XxlJobSpringExecutor xxlJobExecutor() {
		if (enabled) {
			logger.info(">>>>>>>>>>> xxl-job config init.");
			XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
			xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
			xxlJobSpringExecutor.setAppName(appName);
			xxlJobSpringExecutor.setIp(ip);
			xxlJobSpringExecutor.setPort(port);
			xxlJobSpringExecutor.setAccessToken(accessToken);
			xxlJobSpringExecutor.setLogPath(logPath);
			xxlJobSpringExecutor.setLogRetentionDays(logRetentionDays);

			return xxlJobSpringExecutor;
		}
		return null;
	}

}
```
### 步骤四：开发任务

```
@JobHandler(value="demoJobHandler")
@Component
public class DemoJobHandler extends IJobHandler {
    @Override
	public ReturnT<String> execute(String param) throws Exception {
	  XxlJobLogger.log("XXL-JOB,wuchanghao Hello World.");
	  for (int i = 0; i < 5; i++) {
		XxlJobLogger.log("beat at:" + i);
		TimeUnit.SECONDS.sleep(2);
	  }
	  return SUCCESS;
	}
}
```

## 3 调度平台配置

### 新建项目
项目管理：不同执行器可以配置一个项目绑定

![输入图片说明](https://images.gitee.com/uploads/images/2021/0226/091840_6df92d03_346157.png "屏幕截图.png")

### 新增成员

![新增成员](https://images.gitee.com/uploads/images/2021/0226/091857_c905e6da_346157.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/101946_7859e87f_346157.png "屏幕截图.png")

成员角色：编辑者（可对该项目下任务进行操作权限），	观察者（只能查看权限）

> 这个调度平台有两条权限体系，菜单和操作按钮可以通过 `菜单-角色-用户` 来控制，任务查看操作可以通过`项目-成员-角色`来控制

### 添加执行器

![输入图片说明](https://images.gitee.com/uploads/images/2021/0226/091946_045732f2_346157.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0226/091957_7a6a79d5_346157.png "屏幕截图.png")

添加执行器：执行器指的可以互相连接标识，你开发执行器一定要和当前`appName`保持一致，`注册方式`可以选择`自动注册`,任务调度平台可以同步你的机器IP; `手动注册`需要录入机器地址。

### 添加任务

![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/102023_33a4a6a6_346157.png "屏幕截图.png")

## maven 发版说明
```
//升级父版本号
 mvn versions:set -DnewVersion=1.0.1-SNAPSHOT
//升级模块版本号
 mvn -N versions:update-child-modules
//去掉备份pom.xm.versionsBackup
 mvn versions:commit
//提交私服
mvn celan deploy
```

# 上线主要问题

1、 上线日志如果比较多，会造成首页打开比较慢，可以定时执行清除日志即可。

2、redis 使用的cluster方式，可以参考若依项目改造成自己想要的单机的即可。

# 版本改版

## 2018-12-20

#### V1.0.0 
- 将开源项目改造成符合公司体系下项目

## 2019-1-20
#### V1.0.1 
- 1. 集成xxl-job项目，改造界面，改造权限控制
- 2. 增加任务调度系统xxl-job
- 3. 新增在线运行脚本，支持指定版本jdk

## 2019-1-21

- 增加windows本地打包命令程序 `/bin `: `clean.bat` 和 `package.bat` 双击两个运行脚本就行
- 增加liunx的shell部署方式，部署运行 `./dlbe.sh start` or `./dlbe.sh stop`

### v1.0.1  2019-02-21

- 支持docker部署部署,在dlbe-admin 目录下执行：`mvn clean package docker:build`
> 本地没有docker环境是远程服务的话，需要整个docker镜像地址

## 2019-02-26

- 支持按项目组建任务，支持该项目组成员才能查看该任务

## #v1.0.2  2019-03-04

- 修复日志分页不正常
- 新增任务管理页面按项目条件查询功能
- 新增日志管理界面按项目条件查询功能

## v1.0.3  2019-03-05

- 新增执行器与项目业务绑定功能
- 优化执行器页面按项目进行查询
- 修改删除任务与项目关系表
- 增加accessoken校验，防止测试使用线上机器
- 优化发邮件功能

## 2019-03-07 

- 新增maven版本升级步骤

## v1.0.4 2019-03-12

- 整合redis会话保持，支持分布式部署

## v1.0.5 2019-03-14

- 优化dlbe-job 菜单权限控制

## v1.0.6 2019-07-11

- 优化添加、修改、查看任务时，只能选择自己下面执行器
- 优化日志查看，只能看到自己底下日志文件

## 2021-03-01
正式开源出来使用


