package com.zaodei.rxjob.common.exception;


/**
 *@Title:业务异常
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    protected final String message;

    public BusinessException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
