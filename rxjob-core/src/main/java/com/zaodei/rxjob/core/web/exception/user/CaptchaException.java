package com.zaodei.rxjob.core.web.exception.user;


/**
 *@Title:验证码错误异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class CaptchaException  extends UserException{
    private static final long serialVersionUID = 1L;

    public CaptchaException()
    {
        super("user.jcaptcha.error", null);
    }
}
