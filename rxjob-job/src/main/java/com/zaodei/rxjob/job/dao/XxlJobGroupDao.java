package com.zaodei.rxjob.job.dao;

import com.zaodei.rxjob.job.admin.core.model.XxlJobGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by xuxueli on 16/9/30.
 */
@Mapper
public interface XxlJobGroupDao {

    public List<XxlJobGroup> findAll();

    public List<XxlJobGroup> findByAddressType(@Param("addressType") int addressType);

    public int save(XxlJobGroup xxlJobGroup);

    public int update(XxlJobGroup xxlJobGroup);

    public int remove(@Param("id") int id);

    public XxlJobGroup load(@Param("id") int id);
    
    public int removeMore(Integer[] ids);
    
    //查询属于自己项目下调度
    public List<XxlJobGroup> selectJobGroupList(XxlJobGroup xxlJobGroup);
}
