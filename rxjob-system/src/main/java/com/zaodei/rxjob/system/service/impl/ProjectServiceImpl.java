package com.zaodei.rxjob.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zaodei.rxjob.system.mapper.ProjectMapper;
import com.zaodei.rxjob.system.domain.Project;
import com.zaodei.rxjob.system.domain.ProjectUser;
import com.zaodei.rxjob.system.service.IProjectService;

/**
 * 项目 服务层实现
 * 
 * @author wuchanghao
 * @date 2019-02-21
 */
@Service
public class ProjectServiceImpl implements IProjectService 
{
	@Autowired
	private ProjectMapper projectMapper;

	/**
     * 查询项目信息
     * 
     * @param projectId 项目ID
     * @return 项目信息
     */
    @Override
	public Project selectProjectById(Long projectId)
	{
	    return projectMapper.selectProjectById(projectId);
	}
	
	/**
     * 查询项目列表
     * 
     * @param project 项目信息
     * @return 项目集合
     */
	@Override
	public List<Project> selectProjectList(Project project)
	{
	    return projectMapper.selectProjectList(project);
	}
	
    /**
     * 新增项目
     * 
     * @param project 项目信息
     * @return 结果
     */
	@Override
	public int insertProject(Project project)
	{
	    return projectMapper.insertProject(project);
	}
	
	/**
     * 修改项目
     * 
     * @param project 项目信息
     * @return 结果
     */
	@Override
	public int updateProject(Project project)
	{
	    return projectMapper.updateProject(project);
	}

	/**
     * 删除项目对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteProjectById(Long id)
	{
		return projectMapper.deleteProjectById(id);
	}

	@Override
	public List<Map<String, Object>> selectUsersById(Long id) {
		return projectMapper.selectUsersById(id);
	}
   
	/**
	 * 删除项目成员
	 */
	@Override
	public int deleteProjectUser(ProjectUser projectUser) {
		return projectMapper.deleteProjectUser(projectUser);
	}
	
	/**
	 *新增项目成员
	 */
	@Override
	public int insertProjectUser(ProjectUser projectUser) {
		return projectMapper.insertProjectUser(projectUser);
	}
    
	/**
	 *修改项目成员角色
	 */
	@Override
	public int updateProjectUser(ProjectUser projectUser) {
		return projectMapper.updateProjectUser(projectUser);
	}
	@Override
	public List<Long> selectUserIdsById(Long id) {
		return projectMapper.selectUserIdsById(id);
	}
	
	@Override
	public Map<String, Object> selectUsersByProjectUser(ProjectUser projectUser) {
		return projectMapper.selectUsersByProjectUser(projectUser);
	}

	@Override
	public List<Project> selectProjects(Project project) {
		 return projectMapper.selectProjects(project);
	}
	
}
